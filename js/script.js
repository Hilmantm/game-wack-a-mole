const tanah = document.querySelectorAll('.tanah');
const tikus = document.querySelectorAll('.tikus');
const tampilanSkor = document.querySelector('.skor');
const pop = document.querySelector('#pop');

let tanahSebelumnya;
let selesai;
let skor;

function randomTanah(tanah) {
    const t = Math.floor(Math.random() * tanah.length);
    const tRandom = tanah[t];
    if(tRandom == tanahSebelumnya) {
        randomTanah(tanah);
    }
    return tRandom;
}

function randomWaktu(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function munculkanTikus() {
    const tRandom = randomTanah(tanah);
    const wRandom = randomWaktu(300, 800);
    tRandom.classList.add('muncul');
    setTimeout(() => {
        tRandom.classList.remove('muncul');
        if(!selesai) {
            munculkanTikus();
        }
    }, wRandom);
}

function mulai() {
    selesai = false;
    skor = 0;
    tampilanSkor.textContent = skor;
    munculkanTikus();
    setTimeout(() => {
        selesai = true;
    }, 10000);
}

function stop() {
    selesai = true;
}

function reset() {
    skor = 0;
    tampilanSkor.textContent = skor;
}

function pukul() {
    skor++;
    this.parentNode.classList.remove('muncul');
    pop.play();
    tampilanSkor.textContent = skor;
}

tikus.forEach(item => {
    item.addEventListener('click', pukul);
})